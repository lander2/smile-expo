<?php

use yii\db\Migration;
use app\models\User;
use app\models\Profile;
use app\models\Message;
use app\models\ContactList;

class m170421_090406_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        // User table
        $this->createTable(User::tableName(), [
            'id' => $this->primaryKey(),
            'email' => $this->string(64)->notNull()->unique(),
            'password' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'last_activity_at' => $this->integer()->notNull(),
            'profile_id' => $this->integer(),
        ], $tableOptions);

        // ContactList table
        $this->createTable(ContactList::tableName(), [
            'sender_id' => $this->integer()->notNull(),
            'recipient_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('contact_list_pk', ContactList::tableName(), ['sender_id', 'recipient_id']);
        $this->addForeignKey('contact_list_sender_id',
                ContactList::tableName(), 'sender_id',
                User::tableName(), 'id',
                'CASCADE', 'CASCADE');
        $this->addForeignKey('contact_list_recipient_id',
                ContactList::tableName(), 'recipient_id',
                User::tableName(), 'id',
                'CASCADE', 'CASCADE');

        // Profile table
        $this->createTable(Profile::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'status' => $this->string()
        ], $tableOptions);

        $this->addForeignKey('user_profile_id',
                User::tableName(), 'profile_id',
                Profile::tableName(), 'id',
                'SET NULL', 'SET NULL');

        // Message table
        $this->createTable(Message::tableName(), [
            'id' => $this->primaryKey(),
            'from_id' => $this->integer(),
            'to_id' => $this->integer(),
            'unread' => $this->boolean()->notNull(),
            'message' => $this->text(),
            'created' => $this->dateTime()->notNull()
        ], $tableOptions);

        $this->addForeignKey('message_from_id',
                Message::tableName(), 'from_id',
                User::tableName(), 'id',
                'CASCADE', 'CASCADE');
        $this->addForeignKey('message_to_id',
                Message::tableName(), 'to_id',
                User::tableName(), 'id',
                'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey('message_from_id', Message::tableName());
        $this->dropForeignKey('message_to_id', Message::tableName());
        $this->dropForeignKey('user_profile_id', User::tableName());
        $this->dropForeignKey('contact_list_sender_id', ContactList::tableName());
        $this->dropForeignKey('contact_list_recipient_id', ContactList::tableName());
        $this->dropTable(Message::tableName());
        $this->dropTable(ContactList::tableName());
        $this->dropTable(Profile::tableName());
        $this->dropTable(User::tableName());

        return true;
    }
}
