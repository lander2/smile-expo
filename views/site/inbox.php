<?php

/* @var $this yii\web\View */
/* @var $recipient app\models\User */
/* @var $messageForm app\models\form\MessageForm */

use yii\helpers\Html;

$this->title = 'Inbox';
?>
<div class="site-inbox">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">

        <div class="col-md-3">
            <?=$this->render('inbox/_list', [
                'contactList' => $contactList,
                'recipient' => $recipient
            ])?>
        </div>

        <div class="col-md-9">
            <?=$this->render('inbox/_messages', [
                'messages' => $messages,
                'recipient' => $recipient,
                'messageForm' => $messageForm
            ])?>
        </div>

    </div>
</div>