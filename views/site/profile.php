<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Profile '.$user->profile->name;
?>
<div class="site-about">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if ($user->id == Yii::$app->user->id) { ?>
        <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($profileForm, 'status')->textInput(['placeholder' => 'Your status']) ?>
            <?= $form->field($profileForm, 'password')->passwordInput() ?>
            <?= $form->field($profileForm, 'password_again')->passwordInput() ?>
            <?= Html::submitButton('Change profile', ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end(); ?>
    <?php } else { ?>
        <p><strong>Status: </strong> <?=$user->profile->status ?: '<i>Not setted</i>'?>
        <p>
            <?php if (Yii::$app->user->getId()) { ?>
                <?php if (Yii::$app->user->identity->inContactList($user)) { ?>
                This user already in your contact list <?=Html::a('[delete]', Url::toRoute(['/site/unlink', 'userId' => $user->id]))?>
                <?php } else { ?>
                This user is not in your contact list <?=Html::a('[add]', Url::toRoute(['/site/link', 'userId' => $user->id]))?>
                <?php } ?>
            <?php } ?>
        </p>
    <?php } ?>

</div>
