<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="container-fluid">

<?php if ($recipient) { ?>

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-10">
            <?= $form->field($messageForm, 'text')
                    ->textInput(['autofocus' => true, 'placeholder' => 'Message text'])
                    ->label(false) ?>
        </div>
        <div class="col-md-2">
            <?= Html::submitButton('Send', ['class' => 'btn btn-primary btn-block']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <?php foreach ($messages as $msg) { ?>
    <div class="row message">
        <div class="col-md-8<?=($msg->from_id != $recipient->id) ? ' col-md-offset-4 bg-warning' : ' bg-info'?>">
            <p><strong><?=$msg->from->profile->name?></strong> <?=(new DateTime($msg->created))->format('d.m.Y в H:i')?></p>
            <p><?=$msg->message?></p>
        </div>
    </div>
    <?php if ($msg->unread && $msg->from->id == $recipient->id) { $msg->markAsRead(); } ?>
    <?php } ?>

<?php } ?>
</div>


