<?php
use yii\helpers\Url;

$currentRecipient = $recipient ? $recipient->id : null;
?>

<?php foreach ($contactList as $user) : ?>
    <p>
    <?php if ($user->id == $currentRecipient) { ?>
        <strong><?=$user->profile->name?></strong>
    <?php } else { ?>
        <a href="<?=Url::toRoute(['/site/inbox', 'userId' => $user->id])?>"><?=$user->profile->name?></a>
    <?php } ?>
    (<?=$user->unreadCount?>)
    <?=$user->isOnline ? '[Online]' : ''?>
    </p>
<?php endforeach; ?>
