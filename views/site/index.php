<?php

/* @var $this yii\web\View */

use yii\widgets\ListView;

$this->title = 'Offline Messenger';
?>
<div class="site-index">

<?=ListView::widget([
        'dataProvider' => $userDataProvider,
        'itemView' => 'index/_item'
    ]); ?>

</div>
