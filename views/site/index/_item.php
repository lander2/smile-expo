<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="col-md-3">
    <p>
        <strong><?=$model->profile->name?></strong> <?=Html::a('[View profile]', Url::toRoute(['profile', 'userId' => $model->id]))?><br />
        <small><?=$model->profile->status?></small>
    </p>
</div>

