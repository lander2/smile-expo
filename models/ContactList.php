<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%contact_list}}".
 *
 * @property int $sender_id
 * @property int $recipient_id
 *
 * @property User $recipient
 * @property User $sender
 */
class ContactList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contact_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sender_id', 'recipient_id'], 'required'],
            [['sender_id', 'recipient_id'], 'integer'],
            [['recipient_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['recipient_id' => 'id']],
            [['sender_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['sender_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sender_id' => 'Sender ID',
            'recipient_id' => 'Recipient ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient()
    {
        return $this->hasOne(User::className(), ['id' => 'recipient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(User::className(), ['id' => 'sender_id']);
    }

    public static function linkUsers($senderId, $recipientId)
    {
        $success = true;

        if (!self::findOne(['sender_id' => $senderId, 'recipient_id' => $recipientId])) {
            $sussess = $success && (new ContactList(['sender_id' => $senderId, 'recipient_id' => $recipientId]))->save();
        }
        if (!self::findOne(['sender_id' => $recipientId, 'recipient_id' => $senderId])) {
            $sussess = $success && (new ContactList(['sender_id' => $recipientId, 'recipient_id' => $senderId]))->save();
        }
        return $sussess;
    }

    public static function unlinkUsers($senderId, $recipientId)
    {
        return self::deleteAll([
            'sender_id' => [$senderId, $recipientId],
            'recipient_id' => [$senderId, $recipientId],
        ]);
    }
}
