<?php

namespace app\models;

/**
 * This is the model class for table "{{%message}}".
 *
 * @property int $id
 * @property int $from_id
 * @property int $to_id
 * @property int $unread
 * @property string $message
 * @property string $created
 * @property User $from
 * @property User $to
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_id', 'to_id', 'unread'], 'integer'],
            [['unread', 'created'], 'required'],
            [['message'], 'string'],
            [['created'], 'safe'],
            [['from_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['from_id' => 'id']],
            [['to_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['to_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTo()
    {
        return $this->hasOne(User::className(), ['id' => 'to_id']);
    }

    /**
     * Mark the message as reading
     */
    public function markAsRead()
    {
        $this->unread = 0;
        $this->save();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        //filter the html special chars
        $this->message = str_replace(['<', '>', "\n"], ['[', ']', '<br />'], $this->message);
        return parent::beforeSave($insert);
    }

    /**
     * Getting all messages beetween users from array $userIds
     *
     * @param arrau $userIds array of user id's
     * @param integer $count maximal count of the messages
     * @return array
     */
    public static function getMessageList($userIds, $count = 128)
    {
        return self::find()->where(['from_id' => $userIds, 'to_id' => $userIds])
                ->with(['from'])
                ->orderBy('created DESC')
                ->limit($count)
                ->all();
    }
}
