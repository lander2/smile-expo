<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ProfileForm extends Model
{
    public $password;
    public $password_again;
    public $status;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['status', 'password'], 'string'],
            ['password_again', 'compare', 'compareAttribute' => 'password', 'operator' => '==', 'message' => 'Passwords will be equivalents'],
        ];
    }

    public function save()
    {
        if (($user = Yii::$app->user->identity) && ($profile = $user->profile)) {
            $profile->status = $this->status;
            $success = $profile->save();

            if (!empty($this->password) && $this->validate()) {
                $user->setPassword($this->password);
                $success = $success && $user->save();
                $this->password = null;
                $this->password_again = null;
            }

            return $success;
        }

        throw new \yii\web\NotFoundHttpException('Can\'t find the user profile');
    }
}
