<?php

namespace app\models\form;

use yii\base\Model;
use app\models\User;
use app\models\Profile;

class RegistrationForm extends Model
{
    public $email;
    public $password;
    public $password_again;
    public $name;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'password', 'password_again', 'name'], 'required'],
            ['password_again', 'compare', 'compareAttribute' => 'password', 'operator' => '==', 'message' => 'Passwords will be equivalents'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => 'This user is already registered'],
            ['email', 'email'],
        ];
    }

    /**
     *
     * @return boolean
     */
    public function Register()
    {
        $user = new User(['email' => $this->email]);
        $user->setPassword($this->password);

        if ($this->validate() && $user->save()) {
            $user->profile_id = $this->createProfile($user)->id;
            return $user->save();
        }
        return false;
    }

    /**
     *
     * @param User $user
     * @return Profile
     * @throws \yii\web\ServerErrorHttpException
     */
    public function createProfile(User $user)
    {
        $profile = new Profile(['name' => $this->name]);
        if ($profile->save()) {
            return $profile;
        }

        $user->delete();
        throw new \yii\web\ServerErrorHttpException('Can\t create user-profile. Registration fail');
    }
}
