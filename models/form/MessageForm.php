<?php

namespace app\models\form;

use Yii;
use yii\base\Model;
use app\models\Message;

class MessageForm extends Model
{
    public $text;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
        ];
    }

    /**
     * Sending message $text to user $userTo
     *
     * @param \app\models\User $userTo
     * @return boolean
     */
    public function send(\app\models\User $userTo)
    {
        $message = new Message([
            'from_id' => Yii::$app->user->id,
            'to_id' => $userTo->id,
            'unread' => true,
            'message' => $this->text,
            'created' => date('Y-m-d H:i:s')
        ]);

        return $message->save();
    }
}
