<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Message;
use yii\data\ActiveDataProvider;
use app\models\form\MessageForm;
use app\models\form\ProfileForm;
use app\models\ContactList;

class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'profile'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                    [
                        'actions' => ['inbox', 'link', 'unlink'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'userDataProvider' => new ActiveDataProvider([
                'query' => User::find()->where(['IS NOT', 'profile_id', null]),
                'pagination' => [
                    'pageSize' => 120
                ]
            ])
        ]);
    }

    /**
     * Displays profile page.
     *
     * @return string
     */
    public function actionProfile($userId = null)
    {
        $user = $userId ? User::findOne($userId) : Yii::$app->user->identity;
        if (!$user) {
            throw new \yii\web\NotFoundHttpException('Can\'t find user');
        }

        $profileForm = new ProfileForm();
        $profileForm->status = $user->profile->status;

        if (Yii::$app->request->isPost && $profileForm->load(Yii::$app->request->post())) {
            if ($profileForm->save()) {
                return $this->refresh();
            }
        }

        return $this->render('profile', [
            'user' => $user,
            'profileForm' => $profileForm
        ]);
    }

    /**
     * Display chats-interface
     *
     * @param type $userId
     * @return type
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionInbox($userId = null)
    {
        $user = Yii::$app->user->identity;
        $contactList = $user->contactList;
        $messageForm = new MessageForm();
        $recipient = null;
        $messages = null;

        if ($userId) {
            $recipient = $this->findUser($userId);
            $messages = Message::getMessageList([$recipient->id, $user->id]);

            if (!$user->inContactList($recipient)) {
                throw new \yii\web\NotFoundHttpException('You can\'t sending messages to this user');
            }

            if (Yii::$app->request->isPost && $messageForm->load(Yii::$app->request->post()) && $messageForm->send($recipient)) {
                return $this->refresh();
            }
        }

        return $this->render('inbox', [
            'contactList' => $contactList,
            'messages' => $messages,
            'recipient' => $recipient,
            'messageForm' => $messageForm
        ]);
    }

    /**
     * adding user to contact list of current user
     *
     * @param integer $userId
     * @return boolean
     */
    public function actionLink($userId)
    {
        ContactList::linkUsers(Yii::$app->user->id, $this->findUser($userId)->id);
        return $this->redirect(\yii\helpers\Url::toRoute(['profile', 'userId' => $userId]));
    }

    /**
     * Delete user from contact list
     *
     * @param integer $userId
     * @return boolean
     */
    public function actionUnlink($userId)
    {
        ContactList::unlinkUsers(Yii::$app->user->id, $this->findUser($userId)->id);
        return $this->redirect(\yii\helpers\Url::toRoute(['profile', 'userId' => $userId]));
    }

    /**
     * finding model of User
     *
     * @param integer $userId
     * @return app\models\User
     * @throws \yii\web\NotFoundHttpException
     */
    private function findUser($userId)
    {
        if ($user = User::findOne($userId)) {
            return $user;
        }

        throw new \yii\web\NotFoundHttpException('User does not exist');
    }
}
